import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AtendeeForm'
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import AttendeesList from './AttendeesList';
import MainPage from './MainPage';
import PresentationForm from './PresentationFom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className='container'>
      <Routes>
          <Route index element={<MainPage />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="presentations/new" element={<PresentationForm />} />
          <Route path="attendees"
            element={<AttendeesList attendees={props.attendees} />} />
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
