import React from 'react';

class LocationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state ={
            name: '',
            starts: '',
            ends: '',
            max_attendees: '',
            max_presentations: '',
            locations: []
        };
        this.state = {states: []};
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data);
        delete data.locations;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            // formTag.reset();
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_attendees: '',
                max_presentations: '',
                location: '',
            };
        
          this.setState(cleared);
        }
      }

    handleNameChange(event) {
    const value = event.target.value;
    this.setState({ name: value });
    }

    handleStartChange(event) {
    const value = event.target.value;
    this.setState({ starts: value });
    }

    handleEndChange(event) {
    const value = event.target.value;
    this.setState({ ends: value });
    }

    handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({ description: value });
    }

    handleMaxPresentationChange(event) {
    const value = event.target.value;
    this.setState({ max_presentations: value });
    }

    handleMaxAttendeesChange(event) {
    const value = event.target.value;
    this.setState({ max_attendees: value });
    }

    handleLocationChange(event) {
    const value = event.target.value;
    this.setState({ location: value });
    }
    
    async componentDidMount() {
      const url = 'http://localhost:8000/api/locations/';
  
      const response = await fetch(url);
  
      if (response.ok) {
        const data = await response.json();
        this.setState({locations: data.locations})
      }
    }
    render() {
        return (
            <div class="row">
                <div class="offset-3 col-6">
                <div class="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form id="create-conference-form">
                    <div class="form-floating mb-3">
                        <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" class="form-control" />
                        <label for="name">Name</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input onChange={this.handleStartChange} placeholder="Start date" required type="date" name="starts" id="starts" class="form-control" />
                        <label for="starts">Starts</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input onChange={this.handleEndChange} placeholder="End date" required type="date" name="ends" id="ends" class="form-control" />
                        <label for="ends">Ends</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input onChange={this.handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" class="form-control" />
                        <label for="description">Description</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input onChange={this.handleMaxPresentationChange} placeholder="Maximum presentation" required type="number" name="max_presentations" id="max_presentations" class="form-control"/>
                        <label for="max_presentations">Maximum presentation</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input onChange={this.handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" class="form-control" />
                        <label for="max_attendees">Maximum attendees</label>
                    </div>
                    <div class="mb-3">
                        <select required id="location" name="location" class="form-select">
                        <option selected value="">Choose a location</option>
                        </select>
                    </div>
                    <button class="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
      }
    }

export default LocationForm;